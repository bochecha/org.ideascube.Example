import json
import logging.config

import plaster


class Loader(plaster.ILoader):
    def __init__(self, uri):
        app_name = uri.path
        path = f'/etc/ideascube/{app_name}.json'

        self.uri = uri  # TODO: Need a new URI!
        self._settings = {
            'app:main': {
                'pyramid.default_locale_name': 'en',
            },
            'version': 1,  # Required for logging.config.dictConfig()
            'formatters': {
                'generic': {
                    'format': '%(asctime)s %(levelname)-5.5s [%(name)s:%(lineno)s][%(threadName)s] %(message)s',
                },
            },
            'handlers': {
                'console': {
                    'class': 'logging.StreamHandler',
                    'stream': 'ext://sys.stderr',
                    'formatter': 'generic',
                },
            },
            'loggers': {
                'ideascube_example': {  # TODO: Make that better
                    'level': 'DEBUG',
                    'handlers': ['console'],
                    'propagate': False,
                }
            },
            'root': {
                'level': 'INFO',
                'handlers': ['console'],
            },
        }

        if uri.scheme == 'ideascube+debug':
            self._settings['app:main'].update({
                'pyramid.reload_templates': True,
                'pyramid.debug_authorization': True,
                'pyramid.debug_notfound': True,
                'pyramid.debug_routematch': True,
                'pyramid.includes': [
                    'pyramid_debugtoolbar',
                ],
            })

        try:
            with open(self.uri.path, 'r') as f:
                # TODO: Something nicer than JSON for the admins?
                self._settings.update(json.load(f))

        except FileNotFoundError:
            # No custom settings for this app
            pass

    def get_sections(self):
        return self._settings.keys()

    def get_settings(self, section=None, defaults=None):
        settings = {}

        if defaults is not None:
            settings.update(defaults)

        if section is not None:
            settings.update(self._settings.get(section, {}))

        return settings

    def setup_logging(self, defaults=None):
        settings = dict(self._settings)

        if defaults is not None:
            settings.update(defaults)

        logging.config.dictConfig(settings)
