from pyramid.config import Configurator


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    config.include('pyramid_jinja2')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.scan()
    return config.make_wsgi_app()


def cli():
    import importlib
    import plaster
    import waitress

    app_name = 'org.ideascube.Example'
    app_module = 'ideascube_example'

    loader = plaster.get_loader(f'ideascube://{app_name}')
    loader.setup_logging()
    settings = loader.get_settings(section='app:main')

    app = importlib.import_module(app_module).main({}, **settings)
    waitress.serve(app)
