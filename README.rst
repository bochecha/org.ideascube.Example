*Note: This is an attempt at re-imagining the way we build and distribute
Ideascube. It is an experiment, and might lead nowhere. Do not rely on anything
here.*

org.ideascube.Example
=====================

This repository contains an example application to demonstrate how we could
build and distribute Ideascube.

Hacking on the app
------------------

You will need to install `BuildStream`_ first.

After cloning this repository, build the app::

    $ puco dev build

To run the unit tests, run::

    $ puco dev test

To run the app, run::

    $ puco dev run

Installing the app
------------------

You will need to install puco first::

    $ wget https://web1.kymeria.6clones.net/ideascube/puco
    $ cp puco /a/directory/in/your/PATH/

The app is published to an `OSTree`_ repository. To install it::

    $ sudo puco remotes add ideascube https://web1.kymeria.6clones.net/ideascube/repo-apps
    $ sudo puco install ideascube org.ideascube.Example

Then you can run the app with::

    $ puco run org.ideascube.Example ideascube

.. _BuildStream: https://buildstream.gitlab.io/buildstream/
